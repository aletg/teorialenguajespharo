Class {
	#name : #Parser2,
	#superclass : #Parser,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241bis'
}

{ #category : #production }
Parser2 >> initialProduction [

	self sProduction.
]

{ #category : #production }
Parser2 >> sProduction [

	(self lookahead = $() ifTrue: [ 
		self match: $(.
		self sProduction.
		self match: $).
		self sProduction.
	].
]
