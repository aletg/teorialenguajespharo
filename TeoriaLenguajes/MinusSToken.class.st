Class {
	#name : #MinusSToken,
	#superclass : #SToken,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241'
}

{ #category : #'instance creation' }
MinusSToken class >> isFor: aParser [

	^ aParser lookahead = $-.
]

{ #category : #evaluating }
MinusSToken >> value [

	parser match: $-.
	parser sProduction.
	parser sProduction.
]
