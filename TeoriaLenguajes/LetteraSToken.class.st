Class {
	#name : #LetteraSToken,
	#superclass : #SToken,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241'
}

{ #category : #'instance creation' }
LetteraSToken class >> isFor: aParser [

	^ aParser lookahead = $a.
]

{ #category : #evaluating }
LetteraSToken >> value [

	parser match: $a.
]
