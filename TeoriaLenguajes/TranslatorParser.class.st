Class {
	#name : #TranslatorParser,
	#superclass : #Parser,
	#instVars : [
		'traduction'
	],
	#category : #'TeoriaLenguajes-DragonBook-TDS-Exercise231'
}

{ #category : #initialization }
TranslatorParser >> for: aTextToParse [

	traduction := String new writeStream.
	
	super for: aTextToParse.
]

{ #category : #production }
TranslatorParser >> initialProduction [

	self subclassResponsibility.
]

{ #category : #evaluation }
TranslatorParser >> value [

	super value.
	
	^ traduction contents.
]
