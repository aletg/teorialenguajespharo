Class {
	#name : #SmalltalkParser,
	#superclass : #Parser,
	#category : #'TeoriaLenguajes-Guide8'
}

{ #category : #production }
SmalltalkParser >> binaryMessageProduction [

	self unaryMessageProduction.
	[ self lookahead = 'binOp' ]
		whileTrue: [
			self match: 'binOp'.
			self unaryMessageProduction.
		].
]

{ #category : #production }
SmalltalkParser >> initialProduction [

	self keyMessageProduction.
]

{ #category : #production }
SmalltalkParser >> keyMessageProduction [

	self binaryMessageProduction.
	[ self lookahead = 'key' ]
		whileTrue: [
			self match: 'key'.
			self match: ':'.
			self binaryMessageProduction.
		].
]

{ #category : #production }
SmalltalkParser >> unaryMessageProduction [

	self lookahead = 'id'
		ifTrue: [ self match: 'id' ]
		else: self lookahead = $(
		ifTrue: [ self match: $(.
			self objectProduction.
			self match: $) ]
		else: [ self class syntaxError ].
	[ self lookahead = 'id' ] whileTrue: [ self match: 'id' ].
]
