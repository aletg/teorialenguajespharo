"
I store metadata for this package. These meta data are used by other tools such as the SmalllintManifestChecker and the critics Browser
"
Class {
	#name : #ManifestTeoriaLenguajes,
	#superclass : #PackageManifest,
	#category : #'TeoriaLenguajes-Manifest'
}

{ #category : #'code-critics' }
ManifestTeoriaLenguajes class >> ruleGRGuradGuardClauseRuleV1FalsePositive [
	^ #(#(#(#RGMethodDefinition #(#PostfixArithmeticExpressionParser #aProduction #false)) #'2019-07-09T00:03:30.690267-03:00') #(#(#RGMethodDefinition #(#TranslatorParser #firstPrecedenceOperationOneProduction #false)) #'2019-07-09T15:13:48.572249-03:00') #(#(#RGMethodDefinition #(#TranslatorParser #firstPrecedenceOperationTwoProduction #false)) #'2019-07-09T15:14:33.582502-03:00') #(#(#RGMethodDefinition #(#TranslatorParser #secondPrecedenceOperationOneProduction #false)) #'2019-07-09T15:15:51.772413-03:00') #(#(#RGMethodDefinition #(#TranslatorParser #secondPrecedenceOperationTwoProduction #false)) #'2019-07-09T15:16:27.531182-03:00') )
]

{ #category : #'code-critics' }
ManifestTeoriaLenguajes class >> ruleRBGuardingClauseRuleV1FalsePositive [
	^ #(#(#(#RGMethodDefinition #(#TranslatorParser #firstPrecedenceOperationOneProduction #false)) #'2019-07-09T15:13:50.847778-03:00') #(#(#RGMethodDefinition #(#TranslatorParser #firstPrecedenceOperationTwoProduction #false)) #'2019-07-09T15:14:36.291984-03:00') #(#(#RGMethodDefinition #(#TranslatorParser #secondPrecedenceOperationOneProduction #false)) #'2019-07-09T15:15:49.736085-03:00') #(#(#RGMethodDefinition #(#TranslatorParser #secondPrecedenceOperationTwoProduction #false)) #'2019-07-09T15:16:25.326061-03:00') )
]
