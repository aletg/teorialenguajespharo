Class {
	#name : #Excercise241Test,
	#superclass : #TestCase,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241'
}

{ #category : #tests }
Excercise241Test >> test01exerciseATextRecognized [
	
	self assert: (Excercise241 parse: '-a+aa').
]

{ #category : #tests }
Excercise241Test >> test02exerciseASyntaxErrorEmptyString [

	self 
		should: [ Excercise241 parse: ''. ]
		raise: ParseError
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Excercise241 syntaxErrorMessage.
		].
]

{ #category : #tests }
Excercise241Test >> test03exerciseASyntaxErrorWithInvalidToken [

	self 
		should: [ Excercise241 parse: 'bad'. ]
		raise: ParseError
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Excercise241 syntaxErrorMessage.
		].
]

{ #category : #tests }
Excercise241Test >> test04exerciseASyntaxErrorWithValidToken [

	self 
		should: [ Excercise241 parse: 'aa-+'. ]
		raise: ParseError
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Excercise241 syntaxErrorMessage.
		].
]
