Class {
	#name : #Parser,
	#superclass : #Object,
	#instVars : [
		'textToParse'
	],
	#category : #'TeoriaLenguajes-DragonBook-Exercise241bis'
}

{ #category : #parsing }
Parser class >> parse: aTextToParse [

	^ (self new for: aTextToParse) value.
]

{ #category : #'error signalling' }
Parser class >> syntaxError [

	^ ParseError signal: 'Syntax error'
]

{ #category : #initialization }
Parser >> for: aTextToParse [

	textToParse := (aTextToParse, '$') readStream.
	
	"(aTextToParse, '$') asOrderedCollection"
]

{ #category : #production }
Parser >> initialProduction [

	self subclassResponsibility.
]

{ #category : #accessing }
Parser >> lookahead [

	^ textToParse peek.
]

{ #category : #accessing }
Parser >> match: aTerminal [ 

	(self lookahead = aTerminal) ifTrue: [ 
		self next.
	] ifFalse: [
		self class syntaxError.
	].
]

{ #category : #accessing }
Parser >> next [

	textToParse next.
]

{ #category : #evaluation }
Parser >> value [
	
	self initialProduction.
	self match: $$.
	
	^ true
]
