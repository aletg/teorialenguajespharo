Class {
	#name : #SToken,
	#superclass : #Token,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241'
}

{ #category : #'instance creation' }
SToken class >> for: aParser [

	| stateClass |
	
	stateClass := self allSubclasses
								detect: [:anStateClass | anStateClass isFor: aParser. ]
								ifNone: [ InvalidToken. ].
	
	^ stateClass new initializeWith: aParser.
]

{ #category : #'instance creation' }
SToken class >> isFor: aParser [ 

	^ self subclassResponsibility.
]

{ #category : #evaluating }
SToken >> value [

	self subclassResponsibility.
]
