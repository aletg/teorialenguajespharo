Class {
	#name : #Parser3,
	#superclass : #Parser,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241bis'
}

{ #category : #accessing }
Parser3 >> aProduction [

	self lookahead = $0
		ifFalse: [ ^ self ].

	self match: $0.
	self aProduction.
	self match: $1.
]

{ #category : #accessing }
Parser3 >> initialProduction [

	self sProduction.
]

{ #category : #accessing }
Parser3 >> sProduction [

	self match: $0.
	self aProduction.
	self match: $1.
]
