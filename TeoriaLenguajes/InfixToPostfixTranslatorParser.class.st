Class {
	#name : #InfixToPostfixTranslatorParser,
	#superclass : #TranslatorParser,
	#category : #'TeoriaLenguajes-DragonBook-TDS-Exercise231'
}

{ #category : #production }
InfixToPostfixTranslatorParser >> firstPrecedenceOperationOneProduction [
	"F1 -> * Number F1 | lambda"

	(self lookahead = $*) ifTrue: [
		self match: $*.
		self match: $1.
		traduction nextPutAll: '1*'.
		self firstPrecedenceOperationOneProduction.
	].

]

{ #category : #production }
InfixToPostfixTranslatorParser >> firstPrecedenceOperationTwoProduction [
	"F2 -> / Number F1 F2 | lambda"

	(self lookahead = $/) ifTrue: [
		self match: $/.
		self match: $1.
		traduction nextPutAll: '1/'.
		self firstPrecedenceOperationOneProduction.
		self firstPrecedenceOperationTwoProduction.
	].

]

{ #category : #production }
InfixToPostfixTranslatorParser >> initialProduction [
"
infix to prefix
a op b -> op a b

infix grammar construction left-right evaluation
I -> SecondPrecedenceOperation
SecondPrecedenceOperation -> FirstPrecedenceOperation SecondPrecedenceOperator SecondPrecedenceOperation | SecondPrecedenceOperation
FirstPrecedenceOperation -> Number FirstPrecedenceOperator FirstPrecedenceOperation | Number
SecondPrecedenceOperator -> + | -
FirstPrecedenceOperator -> * | /
Number -> number

infix grammar construction right-left evaluation
I -> SecondPrecedenceOperation
SecondPrecedenceOperation -> SecondPrecedenceOperation SecondPrecedenceOperator FirstPrecedenceOperation | SecondPrecedenceOperation
FirstPrecedenceOperation -> FirstPrecedenceOperation FirstPrecedenceOperator Number | Number
SecondPrecedenceOperator -> + | -
FirstPrecedenceOperator -> * | /
Number -> number

add translations
I -> S
S -> { print '+' } S + F
S -> { print '-' } S - F
S -> F
F -> { print '*' } F * Number
F -> { print '/' } F / Number
F -> Number
Number -> { print number } number

NO ES LL(1) por left-recursive

S -> { print '+' } S + F | { print '-' } S - F | F
S -> { print '-' } S - F S1 | F S1
S1 -> + F S1 | lambda

S -> F S1 S2
S1 -> + F S1 | lambda
S2 -> - F S1 S2 | lambda

F -> F * Number | F / Number | Number
F -> F / Number F1 | Number F1
F1 -> * Number F1 | lambda

F -> Number F1 F2
F1 -> * Number F1 | lambda
F2 -> / Number F1 F2 | lambda


S -> F S1 S2
S1 -> + F S1 | lambda
S2 -> - S | lambda

F -> Number F1 F2
F1 -> * Number F1 | lambda
F2 -> / F | lambda

Ahora es LL(1)

Expando F, si no no puedo traducir sin atributos.
Traduzco a postfija, no puedo a prefija sin sintetizados.
S -> F S1 S2
S1 -> + Number { '+' } F1 F2 S1 | lambda
S2 -> - Number { '-' } F1 F2 S1 S2 | lambda

F -> Number F1 F2
F1 -> * Number { '*' } F1 | lambda
F2 -> / Number { '/' } F1 F2 | lambda
"

	self secondPrecedenceOperationProduction.
]

{ #category : #production }
InfixToPostfixTranslatorParser >> secondPrecedenceOperationOneProduction [
	"S1 -> + Number F1 F2 S1 | lambda"

	(self lookahead = $+) ifTrue: [
		self match: $+.
		self match: $1.
		traduction nextPutAll: '1+'.
		self firstPrecedenceOperationOneProduction.
		self firstPrecedenceOperationTwoProduction.
		self secondPrecedenceOperationOneProduction.
	].

]

{ #category : #production }
InfixToPostfixTranslatorParser >> secondPrecedenceOperationProduction [
	"S -> Number F1 F2 S1 S2"

	self match: $1.
	traduction nextPutAll: '1'.
	self firstPrecedenceOperationOneProduction.
	self firstPrecedenceOperationTwoProduction.
	self secondPrecedenceOperationOneProduction.
	self secondPrecedenceOperationTwoProduction.
	
]

{ #category : #production }
InfixToPostfixTranslatorParser >> secondPrecedenceOperationTwoProduction [
	"S2 -> - Number F1 F2 S1 S2 | lambda"

	(self lookahead = $-) ifTrue: [
		self match: $-.
		self match: $1.
		traduction nextPutAll: '1-'.
		self firstPrecedenceOperationOneProduction.
		self firstPrecedenceOperationTwoProduction.
	].

]
