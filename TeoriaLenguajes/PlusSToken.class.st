Class {
	#name : #PlusSToken,
	#superclass : #SToken,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241'
}

{ #category : #'instance creation' }
PlusSToken class >> isFor: aParser [

	^ aParser lookahead = $+.
]

{ #category : #evaluating }
PlusSToken >> value [

	parser match: $+.
	parser sProduction.
	parser sProduction.
]
