Class {
	#name : #PostfixArithmeticExpressionParser,
	#superclass : #Parser,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241bis'
}

{ #category : #production }
PostfixArithmeticExpressionParser >> aProduction [
"
A -> number A Op A | lambda
"

	self lookahead = $1
		ifFalse: [ ^self ].

	self match: $1.
	self aProduction.
	self operationProduction.
	self aProduction.
]

{ #category : #production }
PostfixArithmeticExpressionParser >> initialProduction [
"
+ - */

Precedencia: * / > + -
Sin embargo, notacion postfija respeta el órden.

Operando -> Operando Operando Operacion
Operacion -> + | - | * | /
Operando -> S | number
Ejemplo: 3 2 3 * 3 +
left-recursive
O -> O O Op | Number
Op -> + | - | * | /
Number -> Digit Number'
Number' -> Digit Number'

O -> number A
A -> O Op A | lambda

O -> number A -> number O op A -> number number A op A -> number number O op A op A -> number number number A op A op A -> number number number op op

O -> O O Op -> O O O Op Op -> number number number Op Op

O -> number A
A -> number A Op A | lambda
"

	self operandProduction.
]

{ #category : #production }
PostfixArithmeticExpressionParser >> operandProduction [

	self match: $1.
	self aProduction.
]

{ #category : #production }
PostfixArithmeticExpressionParser >> operationProduction [
"
Operation -> + | - | * | /
"

	self match: (
		self lookahead case
			equalTo: $+ then: [ $+ ];
			equalTo: $- then: [ $- ];
			equalTo: $* then: [ $* ];
			equalTo: $/ then: [ $/ ];
			default: [ self class syntaxError. ]
	).
]
