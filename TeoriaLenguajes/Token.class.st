Class {
	#name : #Token,
	#superclass : #Object,
	#instVars : [
		'parser'
	],
	#category : #'TeoriaLenguajes-DragonBook-Exercise241'
}

{ #category : #initialization }
Token >> initializeWith: aParser [

	parser := aParser.
]

{ #category : #evaluating }
Token >> value [ 

	self subclassResponsibility.
]
