Class {
	#name : #Parser1,
	#superclass : #Parser,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241bis'
}

{ #category : #production }
Parser1 >> initialProduction [ 

	self sProduction.
]

{ #category : #production }
Parser1 >> sProduction [

	self lookahead case
		equalTo: $+
			then: [
				self match: $+.
				self sProduction.
				self sProduction. ];
		equalTo: $-
			then: [
				self match: $-.
				self sProduction.
				self sProduction. ];
		equalTo: $a
			then: [ self match: $a ];
		default: [ self class syntaxError. ].
]
