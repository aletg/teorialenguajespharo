Class {
	#name : #InvalidToken,
	#superclass : #Token,
	#category : #'TeoriaLenguajes-DragonBook-Exercise241'
}

{ #category : #evaluating }
InvalidToken >> value [

	parser syntaxErrorMessage.
]
