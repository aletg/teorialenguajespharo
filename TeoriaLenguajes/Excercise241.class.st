Class {
	#name : #Excercise241,
	#superclass : #Object,
	#instVars : [
		'textToParse',
		'lookahead'
	],
	#category : #'TeoriaLenguajes-DragonBook-Exercise241'
}

{ #category : #'error messages' }
Excercise241 class >> cantFindProductionErrorMessage [
	^ 'I can', $' asString, 't find a production with first that match the next token'.
]

{ #category : #parsing }
Excercise241 class >> parse: aText [

	| parser |

	parser := self new withText: aText.

	^ parser parse.
]

{ #category : #error }
Excercise241 class >> syntaxErrorMessage [
	^ 'Syntax error'.
]

{ #category : #accessing }
Excercise241 >> lookahead [

	^ textToParse peek.
]

{ #category : #accessing }
Excercise241 >> match: aToken [

	(aToken = self lookahead) ifTrue: [ 
		self next.
	] ifFalse: [ 
		self syntaxErrorMessage.
	].
]

{ #category : #accessing }
Excercise241 >> next [ 

	textToParse next.
]

{ #category : #evaluation }
Excercise241 >> parse [

	self sProduction.
	self match: $$.
	
	^ true.
]

{ #category : #production }
Excercise241 >> sProduction [
	
	(SToken for: self) value.
	
]

{ #category : #'error signalling' }
Excercise241 >> syntaxErrorMessage [

	ParseError signal: self class syntaxErrorMessage.
]

{ #category : #'initialization - private' }
Excercise241 >> withText: aTextToParse [

	textToParse := (aTextToParse, '$') readStream.

	" String streamContents: [ :stream |
		stream nextPutAll: aTextToParse.
		stream nextPutAll: '$'.
	].
	textToParse := textToParse readStream."
]
